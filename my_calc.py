from operation import *

def razbienie(stroka) :
    return stroka.split()

def do_float(a) :
    try :
        return float(a)
    except ValueError :
        return 'fail'

def Memory(a) :
    if a == None :
        return 'fail'
    else :
        return a

def HELP() :
    fail = open('help.txt')
    for line in fail :
        try :
            print(line)
        except UnicodeEncodeError :
            print('блин блинский машина не хочет печатать русский текст')
    fail.close()

def MyCalc() :
    import sys

    memory = None
    #operation = {'+': 'add', '-': 'divide', '*': 'multiply', '/': 'divide', '^': 'stepene', '//': 'zel', '%': 'ost'}
    operation = set(['+','-','*','/','^','//','%'])
    result = None

    for line in sys.stdin :
        line = line[:-1]

        if line[0] == '1' :
            try :
                _, a, operator, b = razbienie(line)
				
                if a == 'M' :
                    a = Memory(memory)
					
                if b == 'M' :
                    b = Memory(memory)
					
                if a == 'fail' or b == 'fail' :
                    print('..error: неверный ввод или пустая память')
					
                else :
                    a, b = do_float(a), do_float(b)

                    if a == 'fail' or b == 'fail' :
                        print('..error: неверный ввод')

                    else :
                        if operator in operation :
						
                            if operator == '+' :
                                result = add(a, b)
								
                            elif operator == '-' :
                                result = divide(a, b)
								
                            elif operator == '*' :
                                result = multiply(a, b)
								
                            elif operator == '/' :
                                result = chastnoe(a, b)
								
                            elif operator == '//' :
                                result = zel(a, b)
								
                            elif operator == '%' :
                                result = ost(a,b)
								
                            elif operator == '^' :
                                result = stepene(a, b)
								
                            print('result: ', result)

                        else :
                            print('..error: оператор не найден')

            except ValurError :
                print('..error: неправильный ввод')

        elif line[0] == '2' :
            _, func = line.split()

            if func == 'check_mem' :
                print(memory)

            elif func == 'clear_mem' :
                memory = None

            elif func == 'add_mem' :
                memory = result
				
                if result == None :
                    print('..error: не существует последнего результата')
					
            else :
                print('..error: функция не найдена')

        else :
            line = line.split()

            if line[0] == 'exit' or line[0] == 'exit\n' :
                break

            elif line[0] == 'help' or line[0] == 'help\n' :
                HELP()

            else :
               print('..error: неправильный ввод')

if __name__ == '__main__' :
    MyCalc()
